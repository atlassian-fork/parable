# Parable

Parable is a utility to help speed up Babel compiles by running Babel in parallel across multiple processes using 
the NodeJS cluster functionality.

For more information see the [Parable core README](packages/parable/README.md).