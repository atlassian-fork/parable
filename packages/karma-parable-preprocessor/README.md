# Parable Karma Preprocessor

Karma preprocessor for use with [Parable](https://www.npmjs.com/package/parable).
See the [Parable README](https://bitbucket.org/atlassian/parable/src/master/README.md) for more information about the 
options you can pass to Parable.

This documentation is still a work in progress.

## Usage

Need to flesh this out a lot, but basically you need to add a preprocessor to your Karma config:

```js
{
    "preprocessors": {
        "src/**/*.+(js|jsx)": ["parable"]
    }
}
```