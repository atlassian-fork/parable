const cluster = require('cluster');
const fs = require('fs');
const winston = require('winston');

const config = require('./config');
const processor = require('./processor');

function worker(files, options) {
    const promises = files.map(filename => processor.transpile(filename, options));

    Promise.all(promises)
        .then(results => {
            winston.debug(`Worker complete`);
            if (options.output === config.Output.FILE) {
                // scrub file data
                process.send(results.map(result => Object.assign({}, result, { code: null, map: null })));
            } else {
                process.send(results);
            }
        })
        .catch(err => {
            // the message is serialized to a JSON string, so we lose things like the real stack here, as well as the
            // fact that this is a ParableError instance
            err._error = true;
            process.send(err);
        });
}

if (cluster.isWorker) {
    const options = JSON.parse(process.env.options);

    winston.level = options.logLevel;

    winston.debug(`Starting worker, waiting for files...`);

    process.on('message', (msg) => {
        if (msg.hasOwnProperty('type') && msg.type === 'files') {
            const files = msg.data;
            winston.debug(`Received ${files.length} files, starting work`);
            worker(files, options);
        }
    });

}

module.exports._worker = worker;