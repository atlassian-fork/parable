'use strict';
const cluster = require('cluster');
const fs = require('fs-extra');
const path = require('path');
const winston = require('winston');

const config = require('./config');
const ParableError = require('./error');

const SCALE = 0.5;

function _master(files, options) {
    return new Promise((resolve, reject) => {
        const workerOptions = Object.assign({}, config.defaultOptions, options);
        winston.level = workerOptions.logLevel;
        workerOptions.processes = Math.max(workerOptions.processes || Math.floor(require('os').cpus().length * workerOptions.scale), 1);

        if (workerOptions.output === config.Output.FILE && !workerOptions.outputDir) {
            workerOptions.outputDir = tmp.dirSync().name;
        }

        if (workerOptions.outputDir) {
            fs.mkdirpSync(workerOptions.outputDir);
        }

        if (!Array.isArray(workerOptions.base)) {
            workerOptions.base = [workerOptions.base];
        }

        winston.info(`Starting master with ${files.length} files, and ${workerOptions.processes} processes`);

        let buckets = [];
        for (let i = 0; i < files.length; i++) {
            const bucket = i % workerOptions.processes;
            if (bucket >= buckets.length) {
                buckets.push([]);
            }
            buckets[bucket].push(files[i]);
        }
        let workerCount = 0;
        let workerResponses = [];
        for (let i = 0; i < buckets.length; i++) {
            winston.info(`Starting worker for bucket ${i} with ${buckets[i].length} files`);

            cluster.setupMaster({
                exec: path.resolve(__dirname + '/worker.js'),
                args: [
                    JSON.stringify(workerOptions)
                ]
            });

            const env = Object.assign({}, workerOptions.env, {
                options: JSON.stringify(workerOptions)
            });

            const worker = cluster.fork(env);

            workerCount++;
            worker.on('online', () => {
                winston.debug(`Sending ${buckets[i].length} files to worker ${i}...`);
                worker.send({
                    type: 'files',
                    data: buckets[i]
                });
            });

            worker.on('message', (msg) => {
                workerResponses = workerResponses.concat(msg);
                workerCount--;
                worker.disconnect();

                if (workerCount === 0) {
                    const errors = workerResponses
                        .filter(file => file.hasOwnProperty('_error'))
                        .map(error => new ParableError(error));

                    if (errors.length === 0) {
                        return resolve(workerResponses);
                    } else {
                        return reject(errors);
                    }
                }
            });
        }
    });
}

module.exports = _master;
