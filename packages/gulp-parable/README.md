# Parable Gulp Task

Gulp task for use with [Parable](https://www.npmjs.com/package/parable).
See the [Parable README](https://bitbucket.org/atlassian/parable/src/master/README.md) for more information about the
options you can pass to Parable.

This documentation is still a work in progress.

## Usage

Gulp only processes a single file at a time, so to take advantage of Parable the pattern to use is to precompile with
Parable, to get the parallelisation benefits on all the files, and then for individual files use Parable to (hopefully)
just return the file that was cached.

```js
const gulp = require('gulp');
const parable = require('gulp-parable');

const parableOpts = {
    base: 'src',
    outputDir: 'target/parable',
    babelOptions: {
        // ...
    }
};

const JS_GLOB = ['src/**/*.js'];

gulp.task('precompile-js', parable.precompile(JS_GLOB, parableOpts));

gulp.task('js', ['precompile-js'], function (cb) {
    return gulp.src(JS_GLOB)
        .pipe(parable.transform(parableOpts))
        .on('error', function (error) {
            parable.logError(error);
            cb(new Error('unable to transpile ' + error.filename));
        })
        .pipe(gulp.dest('target'));
});

gulp.task('watch', ['js'], function () {
    gulp.watch([JS_GLOB], ['js']);
});
```