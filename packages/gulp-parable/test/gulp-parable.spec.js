'use strict';

const chai = require('chai');
const expect = chai.expect;
const File = require('vinyl');
const fs = require('fs-extra');
const parable = require('parable');
const path = require('path');

const gulpParable = require('../index');

chai.use(require('chai-string'));

const outputDir = 'test/fixtures/output';
const config = {
    base: 'test/fixtures',
    outputDir,
    babelOptions: {}
};

function fileFromFixture(filename) {
    const filePath = 'test/fixtures/' + filename;
    return new File({
        path: filePath,
        contents: fs.readFileSync(filePath)
    });
}

function transpileFixture(filename) {
    return parable._processor._readSourceFile('test/fixtures/' + filename, config.babelOptions)
        .then(file => {
            fs.mkdirpSync(outputDir);
            fs.writeFileSync(path.join(outputDir, file.hash + '.js'), 'CODE_MARKER');
        });
}

describe('gulp-parable', () => {
    beforeEach(() => {
        return Promise.all([transpileFixture('foo.js')]);
    });
    afterEach(() => {
        fs.removeSync(outputDir);
    });

    describe('precompile', () => {
        it('precompiles files', done => {
            return gulpParable.precompile(['test/fixtures/**/*.js'], config)(function () {
                expect(fs.readdirSync(outputDir)).to.have.length;
                done();
            });
        });
    });

    describe('transform', () => {
        it('returns a pre-transpiled file', done =>  {
            const file = fileFromFixture('foo.js');

            const stream = gulpParable.transform(config);

            stream.write(file);
            stream.once('data', (file) => {
                expect(file.contents.toString()).to.equal('CODE_MARKER');
                done();
            });
        });

        it("transpiles a file if it isn't already cached", done => {
            const file = fileFromFixture('foo2.js');
            const stream = gulpParable.transform(config);

            let fileCount = 0;
            stream.on('data', (file) => {
                if (fileCount === 1) {
                    return done('unexpected file!');
                }
                fileCount++;
                expect(file.contents.toString()).not.to.equal('CODE_MARKER'); // expect this to be a "real" file
            });
            stream.on('end', () => done());
            stream.on('error', done);

            stream.write(file);
            stream.end();
        });

        it("transpiles with sourcemaps", done => {
            const file = fileFromFixture('foo2.js');
            const stream = gulpParable.transform(Object.assign({}, config, {
                babelOptions: {
                    sourceMaps: 'both'
                }
            }));

            let fileCount = 0;
            stream.on('data', file => {
                fileCount++;
                if (fileCount === 1) {
                    expect(file.path).to.endWith('.js');
                    expect(file.path).not.to.endWith('.map.js');
                } else if (fileCount === 2) {
                    expect(file.path).to.endWith('.js.map');
                } else {
                    done(`Only expected two files but got ${fileCount}`);
                }
            });
            stream.on('end', () => {
                expect(fileCount).to.equal(2);
                done();
            });
            stream.on('error', done);

            stream.write(file);
            stream.end();
        })
    });
});